package ru.repin.stringsort;

import java.util.List;

public interface StringOperations {
    List<String> splitTerm(StringBuilder stringBuilder);

    List<String> sortTerm(StringBuilder stringBuilder);

    String buildSortedString(String string);

    List<String> buildSortedStringList(String string);
}
