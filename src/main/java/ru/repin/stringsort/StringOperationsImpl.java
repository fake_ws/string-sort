package ru.repin.stringsort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class StringOperationsImpl implements StringOperations {
    private Comparator<String> comparator = ((string1, string2) -> {
        if (string1.length() == 1 && string2.length() > 1) {
            if (string1.charAt(0) == string2.charAt(1))
                return -1;
            if (string1.charAt(0) != string2.charAt(1))
                return string1.charAt(0) - string2.charAt(1);
        }
        if (string1.length() == 1 && string2.length() == 1) {
            return string1.charAt(0) - string2.charAt(0);
        }
        if (string1.length() > 1 && string2.length() > 1) {
            return string1.replaceAll("\\+|\\(|\\)", "")
                    .compareTo(string2.replaceAll("\\+|\\(|\\)", ""));
        }
        return 0;
    });

    @Override
    public List<String> splitTerm(StringBuilder s) {
        int countOpen = 0;
        int countClose = 0;

        List<String> stringList = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(')
                countOpen++;
            if (s.charAt(i) == ')')
                countClose++;
            if (countOpen == countClose && countOpen > 0 && countClose > 0) {
                stringList.add(s.substring(s.indexOf("("), i + 1));
                s.delete(s.indexOf("("), i + 1);
                i = 0;
                countOpen = 0;
                countClose = 0;
            }
        }
        for (int j = 0; j < s.length(); j++) {
            if (Character.isLetter(s.charAt(j))) {
                stringList.add(Character.toString(s.charAt(j)));
                s.deleteCharAt(j);
            }
        }
        return stringList;
    }

    @Override
    public List<String> sortTerm(StringBuilder s) {
        List<String> stringList = splitTerm(s);
        List<String> stringListSorted = new ArrayList<>();

        for (int i = 0; i < stringList.size(); i++) {
            int countOpen = 0;
            int countClose = 0;
            for (int j = 0; j < stringList.get(i).length(); j++) {
                if (stringList.get(i).charAt(j) == '(')
                    countOpen++;
                if (stringList.get(i).charAt(j) == ')')
                    countClose++;
            }
            if ((countOpen == 1 && countClose == 1) || (countOpen == 0 && countClose == 0)) {
                char[] chars = stringList.get(i).replaceAll("\\+", "").toCharArray();
                Arrays.sort(chars, 0, chars.length - 1);
                if (chars.length > 1) {
                    stringListSorted.add(String.valueOf(chars).replaceAll("", "+")
                            .replaceAll("\\+\\(\\+", "(").replaceAll("\\+\\)\\+", ")"));
                }
                if (chars.length == 1){
                    stringListSorted.add(String.valueOf(chars));
                }
            }
            if (countOpen > 1 && countClose > 1) {
                List<String> list = sortTerm(new StringBuilder(stringList.get(i).substring(stringList.get(i).
                        indexOf('(') + 1, stringList.get(i).lastIndexOf(')'))));

                int countOneLengthString = 0;
                for (int k = 0; k < list.size(); k++) {
                    if (list.get(k).length() == 1)
                        countOneLengthString++;
                }

                if (countOneLengthString > 0)
                    list.sort(comparator);
                if (countOneLengthString == 0)
                    list.sort(Comparator.naturalOrder());

                String string = list.stream().collect(Collectors.joining("+"));
                stringListSorted.add("(" + string + ")");
            }
        }
        return stringListSorted;
    }

    @Override
    public String buildSortedString(String s) {
        List<String> stringList = new ArrayList<>();
        stringList.addAll(sortTerm(new StringBuilder(s.replaceAll(" ", ""))));

        int countOneLengthString = 0;
        for (int i = 0; i < stringList.size(); i++) {
            if (stringList.get(i).length() == 1)
                countOneLengthString++;
        }

        if (countOneLengthString > 0)
            stringList.sort(comparator);
        if (countOneLengthString == 0)
            stringList.sort(Comparator.naturalOrder());

        for (int i = 0; i < stringList.size(); i++) {
            stringList.set(i, stringList.get(i).replaceAll("\\+", " + "));
        }
        return String.join(" + ", stringList);
    }

    @Override
    public List<String> buildSortedStringList(String fileName) {
        FileOperationsImpl fileOperations = new FileOperationsImpl();
        List<String> stringList = fileOperations.readFile(fileName);
        List<String> sortedStringList = new ArrayList<>();
        for (String string : stringList) {
            sortedStringList.add(buildSortedString(string));
        }
        return sortedStringList;
    }
}
