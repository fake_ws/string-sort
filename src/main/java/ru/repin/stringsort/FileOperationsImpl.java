package ru.repin.stringsort;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileOperationsImpl implements FileOperations {
    @Override
    public List<String> readFile(String fileName) {
        List<String> stringList = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stringList = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringList;
    }

    @Override
    public void writeFile(String filename, List<String> stringList) {
        try {
            File file = new File(filename);
            if (file.exists())
                file.delete();
            Files.write(Paths.get(filename), stringList, StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
