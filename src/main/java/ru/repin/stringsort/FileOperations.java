package ru.repin.stringsort;

import java.util.List;

public interface FileOperations {
    List<String> readFile(String string);

    void writeFile(String string, List<String> stringList);
}
