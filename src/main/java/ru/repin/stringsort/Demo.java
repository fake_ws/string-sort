package ru.repin.stringsort;

public class Demo {
    public static void main(String[] args) {
        StringOperationsImpl stringOperations = new StringOperationsImpl();
        FileOperationsImpl fileOperations = new FileOperationsImpl();
        fileOperations.writeFile(args[3], stringOperations.buildSortedStringList(args[1]));
    }
}
