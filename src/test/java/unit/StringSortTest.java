package unit;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import ru.repin.stringsort.StringOperationsImpl;

public class StringSortTest {
    private StringOperationsImpl stringOperations = new StringOperationsImpl();

    @Test
    @DisplayName("Сортировка простой строки")
    public void sortTermTestEasy() {
        String stringInput = "C + A";

        String stringOutput = "A + C";

        assertEquals(stringOutput, stringOperations.buildSortedString(stringInput));
    }

    @Test
    @DisplayName("Сортировка строки c элементами в скобках")
    public void sortTermTestBrackets() {
        String stringInput = "D + A + (F + D)";

        String stringOutput = "A + D + (D + F)";

        assertEquals(stringOutput, stringOperations.buildSortedString(stringInput));
    }

    @Test
    @DisplayName("Сортировка строки c элементами в скобках со вложенностью")
    public void sortTermTestBracketLevel() {
        String stringInput = "((A + D) + (A + (D + B)) + C)";

        String stringOutput = "((A + (B + D)) + (A + D) + C)";

        assertEquals(stringOutput, stringOperations.buildSortedString(stringInput));
    }

    @Test
    @DisplayName("Сортировка сложной строки")
    public void sortTermTestHard() {
        String stringInput = "((B + A) + (F + A) + (A + B)) + Y + (D + A + C) + (F + D) + ((X + (Y + A)) + C)";

        String stringOutput = "((A + B) + (A + B) + (A + F)) + (A + C + D) + (((A + Y) + X) + C) + (D + F) + Y";

        assertEquals(stringOutput, stringOperations.buildSortedString(stringInput));
    }
}
